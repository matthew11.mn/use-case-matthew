<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;

class TaskController extends Controller
{
    public function index(){
        $token = Session::get('token');
        if($token == ''){
            return redirect('/login'); 
        }
        return view('task.create');
    }

    public function createTask(Request $request){
        $token = Session::get('token');
        if($token == ''){
            return redirect('/login'); 
        }
        $title = $request->get('title');
        $description = $request->get('description');
        $date = $request->get('date');
        $time = $request->get('time');

        $response = Http::withToken($token)->post("http://52.77.253.103:3000/api/v1/activities", 
            [
                "title" => $title,
                "description" => $description,
                "date" => $date,
                "time" => $time,
            ]);
        if($response->successful()){
            $response->json();
            return redirect('/home')->with('status', 'Berhasil menambah task baru!');
        } else{
            return redirect('/home')->with('error', 'Terjadi kesalahan ketika menambahkan task baru.');
        }
    }

    public function updateView($id){
        $token = Session::get('token');
        if($token == ''){
            return redirect('/login'); 
        }
        $data = Http::withToken($token)->get("http://52.77.253.103:3000/api/v1/activities/$id")->json();
        return view('task.update', compact('data'));
    }
    
    public function updateTask(Request $request){

        $token = Session::get('token');
        if($token == ''){
            return redirect('/login'); 
        }
        $id = $request->get('id');
        $title = $request->get('title');
        $description = $request->get('description');
        $date = $request->get('date');
        $time = $request->get('time');

        $response = Http::withToken($token)->put("http://52.77.253.103:3000/api/v1/activities/".$id, 
            [
                "title" => $title,
                "description" => $description,
                "date" => $date,
                "time" => $time,
            ]);
        if($response->successful()){
            return redirect('/home')->with('status', 'Berhasil mengubah task!');
        } else{
            return redirect('/home')->with('error', 'Terjadi kesalahan ketika mengubah task.');
        }
    }

    public function deleteTask(Request $request){
        try {
            $token = Session::get('token');
            if($token == ''){
                return redirect('home');
            } else{
                $response = Http::withToken($token)->delete("http://52.77.253.103:3000/api/v1/activities/".$request->get('id'));
                // return redirect('home')->with('status', 'Berhasil menghapus task!'); 
            }
        } catch (\Throwable $th) {
            return redirect('home')->with('error','Terjadi kesalahan tidak dapat menghapus data');
        }
    }
}
