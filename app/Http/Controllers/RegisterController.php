<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;

class RegisterController extends Controller
{
    public function index()
    {
        $token = Session::get('token');
        if($token != null){
            return redirect('home'); 
        }
        return view('auth/register');
    }
    
    public function register(Request $request){
        try {
            $username = $request->get('username');
            $email = $request->get('email');
            $password = $request->get('password');
            $confirmPassword = $request->get('confirmPassword');
            if($password == $confirmPassword){
                $response = Http::post("http://52.77.253.103:3000/api/v1/auth/signup", 
                    [
                        "username" => $username,
                        "email" => $email,
                        "password" => $password,
                        "confirm_password" => $confirmPassword,
                    ]);
                if($response->successful()){
                    $response->json();
                    Session::put('token', $response['token']);
                    Session::put('username', $response['user']['username']);
                    return redirect('/home');
                } else{
                    return view('auth/register')->with('error','Gagal melakukan registrasi. Mohon cek kembali email dan password anda');
                }
            }
        } catch (\Throwable $th) {
            return view('auth/register')->with('error','Gagal melakukan registrasi. Mohon cek kembali email dan password anda');
        }
        
    }
}
