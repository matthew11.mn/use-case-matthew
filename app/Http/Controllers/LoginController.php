<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Collection;
use Session;

class LoginController extends Controller
{
    public function index(){
        $token = Session::get('token');
        if($token != null){
            return redirect('home'); 
        }
        return view('auth/login');
    }

    public function login(Request $request){
        try {
            $email = $request->get('email');
            $password = $request->get('password');
            $response = Http::post("http://52.77.253.103:3000/api/v1/auth/login", 
                [
                    "username" => $email,
                    "password" => $password,
                ]);
            if($response->successful()){
                $response->json();
                Session::put('token', $response['token']);
                Session::put('username', $response['user']['username']);

                return redirect('/home')->with('success', "Login success");
            } else{
                return view('auth/login')->with('error','Gagal melakukan login');
            }
        } catch (\Throwable $th) {
            return view('auth/login')->with('error','Gagal melakukan login');
        }
        
    }
}
