@extends('layout.app')
@section('side')
<ul class="list-unstyled menu-categories" id="accordionExample">
    <li class="menu active">
        <a href="{{url('/home')}}" aria-expanded="true" class="dropdown-toggle">
            <div class="active">
                <i data-feather="home"></i>
                <span> Home</span>
            </div>
        </a>
    </li>
    
    <li class="menu">
        <a href="{{url('/task')}}" aria-expanded="false" class="dropdown-toggle">
            <div class="">
                <i data-feather="package"></i>
                <span> Add Task</span>
            </div>
        </a>
    </li>
</ul>
@endsection
@section('content')

<div class="layout-px-spacing">
    <div class="layout-top-spacing">

            @if(session('status'))
            <div class="alert alert-light-success border-0 mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                <strong>Sukses!</strong> {{session('status')}}</button>
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-light-danger border-0 mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                <strong>Error!</strong> {{session('error')}}</button>
            </div>
            @endif

            <ul class="list-group list-group-media">

                @foreach($data as $d)
                
                    <li class="list-group-item list-group-item-action">
                        <div class="row media">
                        
                            <div class="media-body">
                                <h5 class="tx-inverse">Judul : {{ $d['title'] }}</h5>
                                <p class="mg-b-0">Deskripsi : {{ $d['description'] }}</p>
                                <h6 class="mg-b-0">Tanggal : {{ $d['date'] }}</h6>
                                <p class="mg-b-0">Waktu : {{ $d['time'] }}</p>
                            </div>
                            <a href="{{ url('/task/'.$d['id']) }}">
                            <button class="btn btn-warning"><i data-feather="edit"></i> Edit</button>
                            </a>
                            <button class="btn btn-danger" onclick="hapus('{{csrf_token()}}','{{$d['id']}}')"><i data-feather="trash-2"></i> Hapus</button>
                        
                        </div>
                        <div>
                            
                        </div>
                    </li>
                
                @endforeach
            
            </ul>

    </div>
</div>
@endsection
@section('javascript')
<script>
    function hapus(token,id){
        swal({
        title: "Yakin Ingin Menghapus Data? ",
        text: "Jika dihapus data akan Sepenuhnya Hilang",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#FF5722",
        confirmButtonText: "Ya, Hapus!",
        cancelButtonText: "Tidak!",
        closeOnConfirm: false,
        closeOnCancel: true,
        showLoaderOnConfirm: true
    }).then(function (result) {
        if(result.value){
            var act = '/task/delete';
            $.post(act, {

                _token: token,
                id:id,
                },
            function (data) {
                if(data.message != 'error'){
                    swal(
                    'Berhasil!',
                    'Data berhasil dihapus.',
                    'success'
                    ).then(function () {
                        location.reload();
                    })
                }else{
                    swal(
                    'Error!!',
                    'Terjadi kesalahan. Gagal menghapus data',
                    'error'
                    )
                }
            });
        } else if(result.dismiss){
            swal(
                'Hapus Data Dibatalkan!',
                '',
                'error'
                )
        }
    })
}
</script>
@endsection