@extends('layout.app')
@section('side')
<ul class="list-unstyled menu-categories" id="accordionExample">
    <li class="menu">
        <a href="{{url('/home')}}" aria-expanded="false" class="dropdown-toggle">
            <div class="">
                <i data-feather="home"></i>
                <span> Dashboard</span>
            </div>
        </a>
    </li>

    <li class="menu active">
        <a href="{{url('/task')}}" aria-expanded="true" class="dropdown-toggle">
            <div class="active">
                <i data-feather="package"></i>
                <span> Add Task</span>
            </div>
        </a>
    </li>
    
</ul>
@endsection
@section('content')
<div class="layout-px-spacing">
    <div class="row layout-top-spacing">
        <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing">
            <div class="widget-content widget-content-area br-6">
                <div style="margin:20px;">
                <div style="margin-bottom:20px;">
                    <h3>Update Task</h3>
                </div>
                <form enctype="multipart/form-data" method="post" action="{{url('/task/update')}}">
                @method('PUT')
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 mx-auto mb-3">
                            <div class="form-group">
                                <label for="title">Judul</label> <span style="color:red"><b>*</b></span>
                                <input id="title" type="text" name="title" placeholder="Judul" class="form-control" value="{{ $data['title'] }}" required>
                            </div>
                        </div>                                        
                    </div>
                    <div class="row">
                        <div class="col-12 mx-auto mb-3">
                            <div class="form-group">
                                <label for="description">Deskripsi</label> <span style="color:red"><b>*</b></span>
                                <input id="description" type="text" name="description" placeholder="deskripsi" class="form-control" value="{{ $data['description'] }}" required>
                            </div>
                        </div>                                        
                    </div>
                    <div class="row">
                        <div class="col-12 mx-auto mb-3">
                            <div class="form-group">
                                <label for="date">Tanggal</label> <span style="color:red"><b>*</b></span>
                                <input id="date" type="date" name="date" class="form-control" value="{{ $data['date'] }}" required>
                            </div>
                        </div>                                        
                    </div>
                    <div class="row">
                        <div class="col-12 mx-auto mb-3">
                            <div class="form-group">
                                <label for="time">Tanggal Lahir</label> <span style="color:red"><b>*</b></span>
                                <input class="form-control" type="time" id="time" name="time" value="{{ $data['time'] }}" required>
                            </div>
                        </div>                                   
                    </div>
                    <input type="hidden" value="{{ $data['id'] }}" name="id"/>
                </div>
                <a href="{{url('/home')}}" class="btn btn-secondary-light"> Kembali</a>
                <input type="submit" name="submit" value="Simpan" class="btn btn-primary">
                </form>
                
                </div>
            </div>
        </div>
    </div>
</div>

@endsection