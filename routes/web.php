<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\RegisterController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::post('/logout', [ HomeController::class, "logout" ]);
Route::get('/login', [ LoginController::class, "index" ]);
Route::post('/loginprocess', [ LoginController::class, "login" ]);

Route::get('/home', [ HomeController::class, "index" ]);

Route::get('/register', [ RegisterController::class, "index" ]);
Route::post('/register/user', [ RegisterController::class, "register" ]);

Route::get('/task', [ TaskController::class, "index" ]);
Route::post('/task/create', [ TaskController::class, "createTask" ]);
Route::get('/task/{id}', [ TaskController::class, "updateView" ]);
Route::put('/task/update', [ TaskController::class, "updateTask" ]);
Route::post('/task/delete', [ TaskController::class, "deleteTask" ]);
